package DesafioSensedia.Tiago;

import static com.jayway.restassured.RestAssured.given;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.AssertJUnit;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

public class TestSensedia {
	private final String url = "https://www.facebook.com/v3.2/";
	private final String urlGraph = "https://graph.facebook.com/";
	private final String clientId = "502615973563951";
	private final String permissions = "user_posts%2Cuser_likes%2Cuser_status%2Cuser_friends%2Cuser_location%2Cuser_photos%2Cpublish_pages%2Cemail%2Cuser_age_range%2Cuser_birthday%2Cuser_friends%2Cuser_gender%2Cuser_photos%2Cuser_posts%2Cuser_tagged_places%2Cuser_videos%2Cgroups_access_member_info%2Cuser_events%2Cuser_managed_groups%2Cpublish_to_groups%2Cads_management%2Cads_read%2Cbusiness_management%2Cread_audience_network_insights%2Cread_insights%2Cpages_manage_cta%2Cpages_manage_instant_articles%2Cpages_show_list%2Cread_page_mailboxes";
	public String pageAccessToken = "EAAHJIGaQDi8BANchVM3HrZAwPzwbf3i7ENb48MZCincKUNtQRV5kQAlCbwreFec1ezxnzIYB1YZB1Y3VTKUvzzxQaMky7rkCAGhZC9YZCu3ZAZAMZC8NfzeJeAe6ugU2fzZAdkisFajX9wDYUXDZBCv3UGgz5WZCEGOImCMom2YOdyCNADZC1hOqTFBuqGsy9KwZAbZCgZD";
	private final String email = "open_xtcalky_user@tfbnw.net";
	private final String pass = "matera123";
	private String clientSecret = "580298aaf04b50c44ceba8d0f5936683";
	Random random = new Random();
	private final int num = random.nextInt(9999);

	@Test
	public void loginEmptyEmail() {
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(url + "dialog/oauth?response_type=token&client_id=" + clientId
				+ "&client_secret=" + clientSecret + "&redirect_uri=http://localhost:8080&grant_type=" + permissions);
		driver.findElement(By.id("email")).sendKeys("");
		driver.findElement(By.id("pass")).sendKeys("");
		driver.findElement(By.id("loginbutton")).click();
		String msgErro = driver.findElement(By.xpath("//*[@id=\"globalContainer\"]/div[3]/div/div/div")).getText();
		System.out.println(msgErro);

		AssertJUnit.assertEquals(msgErro,
				"O email ou o número de telefone inserido não corresponde a nenhuma conta. Cadastre-se para abrir uma conta.");
		driver.close();
	}

	@Test
	public void loginErrorEmail() {
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(url + "dialog/oauth?response_type=token&client_id=" + clientId
				+ "&client_secret=" + clientSecret + "&redirect_uri=http://localhost:8080&grant_type=" + permissions);
		driver.findElement(By.id("email")).sendKeys("invalidemail");
		driver.findElement(By.id("pass")).sendKeys("");
		driver.findElement(By.id("loginbutton")).click();
		String msgErro = driver.findElement(By.xpath("//*[@id=\"globalContainer\"]/div[3]/div/div/div")).getText();
		System.out.println(msgErro);

		AssertJUnit.assertEquals(msgErro,
				"A senha inserida está incorreta. Esqueceu a senha?");
		driver.close();
	}

	@Test
	public void loginEmptyPass() {
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(url + "dialog/oauth?response_type=token&client_id=" + clientId
				+ "&client_secret=" + clientSecret + "&redirect_uri=http://localhost:8080&grant_type=" + permissions);
		driver.findElement(By.id("email")).sendKeys(email);
		driver.findElement(By.id("pass")).sendKeys("");
		driver.findElement(By.id("loginbutton")).click();
		String msgErro = driver.findElement(By.xpath("//*[@id=\"globalContainer\"]/div[3]/div/div/div")).getText();
		System.out.println(msgErro);

		AssertJUnit.assertEquals(msgErro, "A senha inserida está incorreta. Esqueceu a senha?");
		driver.close();
	}

	@Test
	public void loginErrorPass() {
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(url + "dialog/oauth?response_type=token&client_id=" + clientId
				+ "&client_secret=" + clientSecret + "&redirect_uri=http://localhost:8080&grant_type=" + permissions);
		driver.findElement(By.id("email")).sendKeys(email);
		driver.findElement(By.id("pass")).sendKeys("invalidpass");
		driver.findElement(By.id("loginbutton")).click();
		String msgErro = driver.findElement(By.xpath("//*[@id=\"globalContainer\"]/div[3]/div/div/div")).getText();
		System.out.println(msgErro);

		AssertJUnit.assertEquals(msgErro, "A senha inserida está incorreta. Esqueceu a senha?");
		driver.close();
	}
	
	@Test
	public void loginSuccess() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.facebook.com");
		driver.findElement(By.id("email")).sendKeys(email);
		driver.findElement(By.id("pass")).sendKeys(pass);
		driver.findElement(By.id("loginbutton")).click();
		String msg = driver.findElement(By.xpath("//*[@id=\"u_0_a\"]/div[1]/div[1]/div/a/span/span")).getText();
		System.out.println(msg);
		AssertJUnit.assertEquals(msg, "Open");
		driver.close();
	}
	
	@Test
	public void postPageTimeLineEmptyToken() {
		String messagePost = "teste" + num;
		String message = "{'message': '" + messagePost + "'}";
		String urlPost = "me/feed/?access_token=";
		Response res = 
				given().
				body(message)
				.when().
				contentType(ContentType.JSON)
				.post(urlGraph + urlPost);
		System.out.println(res.asString());

		ExtractableResponse<Response> extract = res.then().contentType(ContentType.JSON).extract();
		String msg = extract.path("error.message");

		Assert.assertEquals(msg, "An active access token must be used to query information about the current user.");
		Assert.assertEquals(res.getStatusCode(), 400);
	}

	@Test
	public void postPageTimeLineInvalidToken() {
		String messagePost = "teste" + num;
		String message = "{'message': '" + messagePost + "'}";
		String urlPost = "me/feed/?access_token=token123456";
		Response res = 
				given()
				.body(message)
				.when()
				.contentType(ContentType.JSON)
				.post(urlGraph + urlPost);
		System.out.println(res.asString());

		ExtractableResponse<Response> extract = res.then().contentType(ContentType.JSON).extract();
		String msg = extract.path("error.message");

		Assert.assertEquals(msg, "Invalid OAuth access token.");
		Assert.assertEquals(res.getStatusCode(), 400);
	}
	
	private ResponsePageTimeLine postPageTimeLine() {
		String messagePost = "teste" + num;
		String message = "{'message': '" + messagePost + "'}";
		String urlPost = "me/feed/?access_token=" + pageAccessToken;
		Response res = 
				given().
				body(message)
				.when()
				.contentType(ContentType.JSON)
				.post(urlGraph + urlPost);
		 String idPost = res
	        	.then()
	            .contentType(ContentType.JSON)
	            .extract()
	            .path("id");
		 
		System.out.println(res.getStatusCode());
		System.out.println(res.asString());
		Assert.assertEquals(res.getStatusCode(), 200);
		return new ResponsePageTimeLine(message, idPost, messagePost);
	}
	
	@Test
	public void postPageTimeLineSuccess() {
		String messagePost = "teste" + num;
		String message = "{'message': '" + messagePost + "'}";
		String urlPost = "me/feed/?access_token=" + pageAccessToken;
		Response res = 
				given().
				body(message)
				.when()
				.contentType(ContentType.JSON)
				.post(urlGraph + urlPost);
		 
		System.out.println(res.getStatusCode());
		System.out.println(res.asString());
		Assert.assertEquals(res.getStatusCode(), 200);
	}

	@Test
	public void updatePostPageSuccess() {
		String messageUpdate = "teste update " + num;
		ResponsePageTimeLine responsePageTimeLine = postPageTimeLine();
		System.out.println("Message Before Update = " + responsePageTimeLine.messagePost);

		String updatedUrl = responsePageTimeLine.idPost + "/?access_token=" + pageAccessToken;
		Response res = 
				given()
				.param("message", messageUpdate)
				.when()
				.contentType(ContentType.JSON)
				.post(urlGraph + updatedUrl);
		System.out.println(res.getStatusCode());	
		Assert.assertEquals(res.getStatusCode(), 200);
		
	}
	
	@Test
	public void deletePostPage() {
		ResponsePageTimeLine responsePageTimeLine = postPageTimeLine();

		String updatedUrl = responsePageTimeLine.idPost + "/?access_token=" + pageAccessToken;
		Response res = 
				given()
				.when()
				.contentType(ContentType.JSON)
				.delete(urlGraph + updatedUrl);
		System.out.println(res.getStatusCode());	
		Assert.assertEquals(res.getStatusCode(), 200);
	}

}
